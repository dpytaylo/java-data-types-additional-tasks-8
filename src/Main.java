import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var number = scanner.nextInt();

        var result = number % 10;
        number /= 10;

        while(number > 0) {
            var n = number % 10;
            number /= 10;

            result *= n;
        }

        System.out.println("Result: " + result);
    }
}